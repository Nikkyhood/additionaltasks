// Даны два целых числа k и n. Верните из функции строку, которая состоит из чисел n, повторяющихся k раз, разделенных пробелом. Известно, что и k и n больше либо равно 1.

// Решение через for и while

function testCycle(k, n) {
    let x = "";
    for(i = 1; i <= k; i++){
        x = x + " "+ n;
    }
    return x;
}


    function testCycle(k, n) {
        var x = " ";
        var i = 0;
        while (i < k) {
            x = x + " " + n;
            i++;
        }
        return x;
    }
