// Найдите сумму  всех целых чисел от 1 до n включительно и верните из функции результат.


// Решение через for

function testCycle(n) {
    let result = 0;
    for(let i = 1; i <= n; i++){
        result += i;
    }
    return result;
}


// Решение тернарником

function testCycle(n) {
    return n + (n > 1 ? testCycle(n - 1) : 0);
}