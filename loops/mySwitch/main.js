let gear = 3;

switch (gear) {
    case 0:
        console.log("Neutral");
        break;
    case 1:
        console.log("Speed from 0 to 10");
        break;
    case 2:
        console.log("Speed from 10 to 20");
        break;
    case 3:
        console.log("Speed from 30 to 50");
        break;
    case 4:
        console.log("Speed from 50 to 80");
        break;
    case 5:
        console.log("Speed from 80 to 200");
        break;
    default:
        console.log("Illegal");
}