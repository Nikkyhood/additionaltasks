const country = prompt("Enter the city");
switch (country) {
    case "Austria":
        alert("Vienna");
        break;
    case "The Bahamas":
        alert("Nassau");
        break;
    case "United States of America":
    case "USA":
        alert("Washington, D.C.");
        break;
    case "Ukraine":
        alert("Kyiv");
        break;
    case "Turkey":
        alert("Ankara");
        break;
    case "United Kingdom":
    case "UK":
        alert("London");
        break;
    default:
        alert("Unknown country");
}

